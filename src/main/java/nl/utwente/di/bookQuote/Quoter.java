package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    private Map<String, Double> bookPrices;

    public Quoter() {
        // Initialize the bookPrices HashMap with the values from Table 1
        bookPrices = new HashMap<>();
        bookPrices.put("1", 10.0);
        bookPrices.put("2", 45.0);
        bookPrices.put("3", 20.0);
        bookPrices.put("4", 35.0);
        bookPrices.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        // Return the price from the HashMap, or 0.0 if not found
        return bookPrices.getOrDefault(isbn, 0.0);
    }
}
